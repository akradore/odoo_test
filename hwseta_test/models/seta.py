from odoo import _, api, fields, models
from odoo.exceptions import UserError
from odoo.osv import expression


class Learners(models.Model):
    _name = "hwseta.learners"
    
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char()

    # _sql_constraints = [
    # ('name', 'unique (name)', 'The name already Exists!'),
    # ]
    institution = fields.Char()
    id_number = fields.Char(store=True, required=True, help="Unique Lot/Serial Number")
    company_id = fields.Many2one(
        "res.company", "Company", required=True, stored=True, index=True
    )
    enrollment_date = fields.Date(string="Date of Enrolment")
    note = fields.Html(string="Description")
    # qualification = fields.Many2one("hwseta.qualifications")
    qualification = fields.One2many('learner.qualifications', 'learner', string='Learners Qualifications',
        )

    # qualification_ids = fields.Many2many('learner.qualifications', 'learners_qualification_rel_table', 'learnerz_id', 'qualification_ids', string="Qualification")

    _sql_constraints = [
        (
            "id_number_uniq",
            "unique (id_number)",
            "The  ID must be unique, this one is already assigned to another Learner.",
        )
    ]
    # qualifications = fields.????
    # >>>>
    state = fields.Selection(
        [
            ("new", "New"),
            ("review", "Under-Review"),
            ("progress", "In-Progress"),
            ("done", "Done"),
            ("withdrawn", "Withdrawn"),
        ],
        string="Status",
        default="new",
    )

    def button_mark_pass(self):
        for rec in self:
            rec.state = 'done'

    def action_confirm(self):
        for rec in self:
            rec.state = 'review'
    
    def button_validate(self):
        for rec in self:
            rec.state = 'progress'
    
    def action_withdraw(self):
        for rec in self:
            rec.state = 'withdrawn'


# >>>>


class Qualifications(models.Model):
    _name = "hwseta.qualifications"

    def _get_total_credits(self):
        for record in self:
            record.total_credit = sum(record.units.mapped('credit'))
        

    name = fields.Char()
    # units = fields.???
    total_credit = fields.Integer(compute=_get_total_credits)
    minimum_credit = fields.Integer(string="Minimum Pass Mark")
    units = fields.One2many('hwseta.units', 'qualification', string='Units', )


class Units(models.Model):
    _name = "hwseta.units"

    name = fields.Char()
    credit = fields.Integer()
    qualification = fields.Many2one("hwseta.qualifications")


class LearnerQualifications(models.Model):
    _name = "learner.qualifications"

    @api.depends('learners_credits', 'minimum_credit')
    def _compute_state(self):
        for rec in self:
            if rec.minimum_credit <= rec.learners_credits:
                rec.state = 'Pass'
            else:
                rec.state = 'Fail'

    def _get_learners_credits(self):
        for record in self:
            # record.learners_credits = 10
            # record.standard_credits = 10
            record.learners_credits = sum(record.learners_units.mapped('learners_credits'))
            record.standard_credits = sum(record.learners_units.mapped('standard_credit'))

    # name = fields.Char()
    qualification = fields.Many2one("hwseta.qualifications")
    code = fields.Char(string="Qulaification Code")
    learner = fields.Many2one("hwseta.learners")
    learners_units = fields.One2many('learner.units', 'qualification', string='Learners Units' )
    minimum_credit = fields.Integer(related ='qualification.minimum_credit', string="Minimum Pass Mark")
    standard_credits = fields.Integer(compute=_get_learners_credits)
    learners_credits = fields.Integer(compute=_get_learners_credits)
    state = fields.Selection(
        [
            ("Pass", "Pass"),
            ("Fail", "Fail"),
        ],
        string="Grade Status",compute= (_compute_state), 
    )


    
class LearnerUnits(models.Model):
    _name = "learner.units"

    master_unit = fields.Many2one("hwseta.units")
    achieved = fields.Boolean()
    qualification = fields.Many2one('learner.qualifications', string='Learners Qualification')
    standard_credit = fields.Integer(related ='master_unit.credit', string='Standard Credit')
    learners_credits  = fields.Integer()

