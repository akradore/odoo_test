{
    'name': 'HWSETA odoo test',
    'description': '''Adds Test stuff''',
    'category': 'HWSETA',
    'version': '12.0.1.0.0',
    'author': 'Dylan Bridge.',
    'website': '',
    'depends': ['base','mail'
                ],
    'data': [
            'views/seta_view.xml',
            'security/ir.model.access.csv',
             ]
}
